import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  StyleSheet,
  PermissionsAndroid,
  ScrollView,
  NativeModules,
  Platform,
} from 'react-native';
import {Text} from 'react-native-paper';
import {Button} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
import {TextInput} from 'react-native-paper';
import {
  RTCPeerConnection,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  mediaDevices,
} from 'react-native-webrtc';

import RNCallKeep from 'react-native-callkeep';
import uuidLib from 'uuid';
import {STUN_SERVER, SOCKET_URL} from './config';

const options = {
  ios: {
    appName: 'Nome do meu app',
  },
  android: {
    alertTitle: 'Permissions required',
    alertDescription: 'This application needs to access your phone accounts',
    cancelButton: 'Cancel',
    okButton: 'ok',
    imageName: 'phone_account_icon',
    additionalPermissions: [PermissionsAndroid.PERMISSIONS.CALL_PHONE],
  },
};

export default function CallScreen({navigation, ...props}) {
  const [userId, setUserId] = useState('');
  const [socketActive, setSocketActive] = useState(false);
  const [calling, setCalling] = useState(false);
  const [localStream, setLocalStream] = useState({toURL: () => null});
  const [remoteStream, setRemoteStream] = useState({toURL: () => null});

  const conn = useRef(new WebSocket(SOCKET_URL));

  const yourConn = useRef(
    new RTCPeerConnection({
      iceServers: [
        {
          urls: STUN_SERVER,
        },
      ],
    }),
  );

  const [callActive, setCallActive] = useState(false);
  const [incomingCall, setIncomingCall] = useState(false);
  const [otherId, setOtherId] = useState('');
  const [callToUsername, setCallToUsername] = useState('');
  const connectedUser = useRef(null);
  const offerRef = useRef(null);

  async function SetAccountTelephoneStandard() {
    const status = await RNCallKeep.hasPhoneAccount();
    if (status == false) {
      const optionsDefaultNumber = {
        alertTitle: 'Standard not defined.',
        alertDescription: 'Set the default phone bill.',
      };

      RNCallKeep.hasDefaultPhoneAccount(optionsDefaultNumber);
    }
  }

  async function display() {
    await SetAccountTelephoneStandard();

    const uuid = uuidLib.v4();

    try {
      RNCallKeep.displayIncomingCall(uuid, 'NATIVE CALL, UHULL!', 'Demo call');
      RNCallKeep.answerIncomingCall(uuid);
    } catch (error) {
      console.log('Error: ', error);
    }
  }

  useEffect(() => {
    AsyncStorage.getItem('userId').then(id => {
      if (id) {
        setUserId(id);
      }
    });

    conn.current.onopen = () => {
      console.log('Connected to the signaling server');
      setSocketActive(true);
    };
    //when we got a message from a signaling server
    conn.current.onmessage = msg => {
      const data = JSON.parse(msg.data);

      console.log('conn.current.onmessage', data);
      switch (data.type) {
        case 'login':
          break;
        //when somebody wants to call us
        case 'offer':
          handleOffer(data.offer, data.name);
          break;
        case 'answer':
          handleAnswer(data.answer);
          break;
        //when a remote peer sends an ice candidate to us
        case 'candidate':
          handleCandidate(data.candidate);
          break;
        case 'leave':
          handleLeave();
          break;
        default:
          break;
      }
    };
    conn.current.onerror = function (err) {
      console.log('Got error', err);
    };
    initLocalVideo();
    registerPeerEvents();

    RNCallKeep.setup(options).then(accepted => {
      RNCallKeep.setAvailable(true);
    });

    RNCallKeep.addEventListener('answerCall', ({callUUID}) => {
      console.log('Receiving a call ', callUUID);

      if (Platform.OS === 'android') {
        const {CallkeepHelperModule} = NativeModules;
        CallkeepHelperModule.startActivity();
        RNCallKeep.endCall(callUUID);
      }
      RNCallKeep.setCurrentCallActive(callUUID);
      acceptCall();
    });

    RNCallKeep.addEventListener('endCall', ({callUUID}) => {
      console.log('User ended the call');
      // handleLeave();
    });

    RNCallKeep.addEventListener(
      'didDisplayIncomingCall',
      ({
        error,
        callUUID,
        handle,
        localizedCallerName,
        hasVideo,
        fromPushKit,
      }) => {
        console.log('Making a call');
      },
    );

    RNCallKeep.addEventListener(
      'didReceiveStartCallAction',
      ({handle, callUUID, name}) => {
        console.log('display');
      },
    );

    RNCallKeep.addEventListener(
      'didPerformDTMFAction',
      ({digits, callUUID}) => {
        console.log('Capturing key:', digits);
      },
    );
  }, []);

  useEffect(() => {
    navigation.setOptions({
      title: 'Your ID - ' + userId,
      headerRight: () => (
        <Button mode="text" onPress={onLogout} style={{paddingRight: 10}}>
          Logout
        </Button>
      ),
    });
  }, [userId]);

  /**
   * Calling Stuff
   */

  useEffect(() => {
    if (socketActive && userId.length > 0) {
      try {
      } catch (err) {
        console.log('InApp Caller ---------------------->', err);
      }
      send({
        type: 'login',
        name: userId,
      });
    }
  }, [socketActive, userId]);

  useEffect(() => {
    if (incomingCall && !callActive) {
      display();
    }
  }, [incomingCall, callActive]);

  const onLogin = () => {};

  // useEffect(() => {
  //   if (!callActive) {
  //     // InCallManager.stop();
  //   } else {
  //     // InCallManager.setSpeakerphoneOn(true);
  //   }
  // }, [callActive]);

  const registerPeerEvents = () => {
    yourConn.current.onaddstream = event => {
      console.log('On Add Remote Stream');
      setRemoteStream(event.stream);
    };

    // Setup ice handling
    yourConn.current.onicecandidate = event => {
      if (event.candidate) {
        send({
          type: 'candidate',
          candidate: event.candidate,
        });
      }
    };
  };

  const initLocalVideo = () => {
    mediaDevices
      .getUserMedia({
        audio: true,
        video: {
          mandatory: {
            minWidth: 500, // Provide your own width, height and frame rate here
            minHeight: 300,
            minFrameRate: 30,
          },
          facingMode: 'user',
          // optional: videoSourceId ? [{sourceId: videoSourceId}] : [],
        },
      })
      .then(stream => {
        // Got stream!
        setLocalStream(stream);

        // setup stream listening
        yourConn.current.addStream(stream);
      })
      .catch(error => {
        // Log error
      });
    // });
  };

  const send = message => {
    if (connectedUser.current) {
      message.name = connectedUser.current;
      // console.log('Connected iser in end----------', message);
    }
    console.log('---Message---', message);
    conn.current.send(JSON.stringify(message));
  };

  const onCall = () => {
    sendCall(callToUsername);
    setTimeout(() => {
      sendCall(callToUsername);
    }, 1000);
  };

  const sendCall = receiverId => {
    setCalling(true);
    const otherUser = receiverId;
    connectedUser.current = otherUser;
    console.log('Caling to', otherUser);
    // create an offer
    yourConn.current.createOffer().then(offer => {
      yourConn.current.setLocalDescription(offer).then(() => {
        console.log('Sending Ofer');
        send({
          type: 'offer',
          offer: offer,
        });
        // Send pc.localDescription to peer
      });
    });
  };

  //when somebody sends us an offer
  const handleOffer = async (offer, name) => {
    console.log(name + ' is calling you.');
    connectedUser.current = name;
    offerRef.current = {name, offer};
    setIncomingCall(true);
    setOtherId(name);
    // acceptCall();
    if (callActive) acceptCall();
  };

  const acceptCall = async () => {
    const name = offerRef.current.name;
    const offer = offerRef.current.offer;
    setIncomingCall(false);
    setCallActive(true);
    console.log('Accepting CALL', name, offer);
    yourConn.current
      .setRemoteDescription(offer)
      .then(function () {
        connectedUser.current = name;
        return yourConn.current.createAnswer();
      })
      .then(function (answer) {
        yourConn.current.setLocalDescription(answer);
        send({
          type: 'answer',
          answer: answer,
        });
      })
      .then(function () {
        // Send the answer to the remote peer using the signaling server
      })
      .catch(err => {
        console.log('Error acessing camera', err);
      });
  };

  //when we got an answer from a remote user
  const handleAnswer = answer => {
    setCalling(false);
    setCallActive(true);
    yourConn.current.setRemoteDescription(new RTCSessionDescription(answer));
  };

  //when we got an ice candidate from a remote user
  const handleCandidate = candidate => {
    setCalling(false);
    // console.log('Candidate ----------------->', candidate);
    yourConn.current.addIceCandidate(new RTCIceCandidate(candidate));
  };

  const onLogout = () => {
    // hangUp();

    handleLeave();

    AsyncStorage.removeItem('userId').then(res => {
      navigation.push('Login');
    });
  };

  const rejectCall = async () => {
    send({
      type: 'leave',
    });
    // ``;
    // setOffer(null);

    // handleLeave();
  };

  const handleLeave = () => {
    send({
      name: userId,
      otherName: otherId,
      type: 'leave',
    });

    setCalling(false);
    setIncomingCall(false);
    setCallActive(false);
    offerRef.current = null;
    connectedUser.current = null;
    setRemoteStream(null);
    setLocalStream(null);
    yourConn.current.onicecandidate = null;
    yourConn.current.ontrack = null;

    resetPeer();
    initLocalVideo();
    // console.log("Onleave");
  };

  const resetPeer = () => {
    yourConn.current = new RTCPeerConnection({
      iceServers: [
        {
          urls: STUN_SERVER,
        },
      ],
    });

    registerPeerEvents();
  };

  /**
   * Calling Stuff Ends
   */

  return (
    <View style={styles.root}>
      <View style={styles.inputField}>
        <TextInput
          label="Enter Friends Id"
          mode="outlined"
          style={{marginBottom: 7}}
          onChangeText={text => setCallToUsername(text)}
        />
        <Text>
          SOCKET ACTIVE:{socketActive ? 'TRUE' : 'FASLE'}, FRIEND ID:
          {callToUsername || otherId}
        </Text>
        <Button
          mode="contained"
          onPress={onCall}
          loading={calling}
          //   style={styles.btn}
          contentStyle={styles.btnContent}
          disabled={!socketActive || callToUsername === '' || callActive}>
          Call
        </Button>
        <Button
          mode="contained"
          onPress={handleLeave}
          contentStyle={styles.btnContent}
          disabled={!callActive}>
          End Call
        </Button>

        <Button
          mode="contained"
          onPress={display}
          contentStyle={styles.btnContent}>
          Test call
        </Button>
      </View>

      <ScrollView style={styles.videoContainer}>
        <View style={[styles.videos, styles.localVideos]}>
          <Text>Your Video</Text>
          <RTCView
            streamURL={localStream ? localStream.toURL() : ''}
            style={styles.localVideo}
          />
        </View>
        <View style={[styles.videos, styles.remoteVideos]}>
          <Text>Friends Video</Text>
          <RTCView
            streamURL={remoteStream ? remoteStream.toURL() : ''}
            style={styles.remoteVideo}
          />
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#fff',
    flex: 1,
    padding: 20,
  },
  inputField: {
    marginBottom: 10,
    flexDirection: 'column',
  },
  videoContainer: {
    flex: 1,
    // minHeight: 450,
  },
  videos: {
    width: '100%',
    flex: 1,
    position: 'relative',
    overflow: 'hidden',

    borderRadius: 6,
  },
  localVideos: {
    height: 220,
    marginBottom: 10,
  },
  remoteVideos: {
    height: 220,
  },
  localVideo: {
    backgroundColor: '#f2f2f2',
    height: '100%',
    width: '100%',
  },
  remoteVideo: {
    backgroundColor: '#f2f2f2',
    height: '100%',
    width: '100%',
  },
});
