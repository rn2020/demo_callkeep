module.exports = {
  root: true,
  extends: '@react-native-community',
  plugins: ['react-hooks'],
  rules: {
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'comma-dangle': 0,
    'react-native/no-inline-styles': 0,
    // 'newline-per-chained-call': 0,
    curly: 0,
    eqeqeq: 0,
    // 'prettier/prettier': ['error', { /*arrowParens: 'avoid',*/ printWidth: 150 }]
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [['~', './src']],
        extensions: ['.js', '.android.js', '.ios.js', '.web.js'],
      },
    },
  },
};
