# Guide install project: Video call

## Get Started

##### Run Server

```
cd server && yarn
yarn start
```


##### Config Domain
```
1) Custom domain: 
- Download ngrok.com: https://ngrok.com/download
- ...(DIR)...\ngrok-stable-windows-amd64\ngrok.exe http 8000

2) Config URL Mobile
-  update URL -> src/config: SOCKET_URL:'wss://xxxx.abc'
```
##### Run Mobile

```
yarn && yarn android
yarn && yarn ios
```

##### Library uses:
```
1) react-native-callkeep
2) react-native-webrtc
3) socket.io-client
```
##### Demo App

![Windows Screenshot](assets/1.jpg)
![Windows Screenshot](assets/2.jpg)
![Windows Screenshot](assets/3.jpg)
![Windows Screenshot](assets/4.jpg)



